// Video Tutorial - https://youtu.be/emyKW9gaobI

class Solution
{
    public:
    //Function to find the first non-repeating character in a string.
    char nonrepeatingCharacter(string S)
    {
        unordered_map<char, int> countMap;
        for(char c : S) {
            ++countMap[c];
        }
        for(char c : S) {
            if(countMap[c] == 1) return c;
        }
        return '$';
    }

};