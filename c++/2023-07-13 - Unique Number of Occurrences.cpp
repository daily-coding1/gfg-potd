// Video Tutorial - https://youtu.be/wA5qpFU0NrE

class Solution
{
    public:
    bool isFrequencyUnique(int n, int arr[])
    {
        unordered_map<int, int> freq;
        for(int i = 0; i < n; i++) ++freq[arr[i]];
        unordered_set<int> seen;
        for(auto& p : freq) {
            int val = p.second;
            // if val is present in seen
            if(seen.find(val) != seen.end()) return false;
            seen.insert(val);
        }
        return true;
    }
};