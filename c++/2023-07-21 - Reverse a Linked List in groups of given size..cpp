// Video Tutorial - https://youtu.be/_-eUsYGQuMM

class Solution
{
    public:
    struct node *reverse (struct node *head, int k)
    {
        struct node *cur = head, *prevHead = NULL;
        while(cur) {
            struct node *tail = moveForward(cur, k-1);
            struct node *nextCur = tail -> next;
            tail -> next = NULL;
            reverseList(cur);
            if(prevHead) prevHead -> next = tail;
            else head = tail;
            prevHead = cur;
            cur = nextCur;
        }
        return head;
    }
    
    // node must be non-NULL
    struct node *moveForward(struct node *node, int steps) {
        while(node -> next && steps--) {
            node = node -> next;
        }
        return node;
    }
    
    void reverseList(struct node *head) {
        struct node *prev = NULL;
        while(head) {
            struct node *next = head -> next;
            head -> next = prev;
            prev = head;
            head = next;
        }
    }
};