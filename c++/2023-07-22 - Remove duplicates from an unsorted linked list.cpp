// Video Tutorial - https://youtu.be/-yfAETHu56s

// Approach - 1
class Solution
{
    public:
    //Function to remove duplicates from unsorted linked list.
    Node * removeDuplicates( Node *head) 
    {
        unordered_set<int> seen;
        Node* prev = NULL;
        Node* cur = head;
        while(cur) {
            Node* nextCur = cur -> next;
            if(seen.find(cur -> data) != seen.end()) {
                head = removeNode(cur, prev, head);
            } else {
                prev = cur;
                seen.insert(cur -> data);
            }
            cur = nextCur;
        }
        return head;
    }
    
    Node* removeNode(Node* cur, Node* prev, Node* head) {
        Node* next = cur -> next;
        if(!prev) {
            cur -> next = NULL;
            free(cur);
            return next;
        }
        prev -> next = next;
        cur -> next = NULL;
        free(cur);
        return head;
    }
};

// Approach - 2
class Solution
{
    public:
    //Function to remove duplicates from unsorted linked list.
    Node * removeDuplicates( Node *head) 
    {
        Node* dummy = new Node(0);
        dummy -> next = head;
        Node* cur = head, *prev = dummy;
        unordered_set<int> seen;
        while(cur) {
            Node* nextCur = cur -> next;
            if(seen.find(cur -> data) != seen.end()) {
                removeNode(prev);
            } else {
                seen.insert(cur -> data);
                prev = cur;
            }
            cur = nextCur;
        }
        Node* ans = dummy -> next;
        dummy -> next = NULL; 
        free(dummy);
        return ans;
    }
    
    void removeNode(Node* prev) {
        Node* cur = prev -> next;
        prev -> next = cur -> next;
        cur -> next = NULL;
        free(cur);
    }
};