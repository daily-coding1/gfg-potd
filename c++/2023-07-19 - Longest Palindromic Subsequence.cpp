// Video Tutorial - https://youtu.be/TH2-7OJKwPo

class Solution{
  public:
    int longestPalinSubseq(string A) {
        int n = A.length();
        string s1 = A;
        reverse(A.begin(), A.end());
        string s2 = A;
        int dp[n+1][n+1];
        // dp[i][j] : Length of the longest subsequence common in s1[0...i-1] ans s2[0...j-1]
        
        // dp[i][j] = 1 + dp[i-1][j-1], if last character of s1[0...i-1] is same as that of s1[0...j-1]
        //          = max(dp[i-1][j], dp[i][j-1]), otherwise

        // Base case
        for(int i = 0; i <= n; i++) dp[i][0] = 0;
        for(int j = 0; j <= n; j++) dp[0][j] = 0;

        for(int i = 1; i <= n; i++) {
            for(int j = 1; j <= n; j++) {
                dp[i][j] = s1[i-1] == s2[j-1] ? 1 + dp[i-1][j-1] : max(dp[i][j-1], dp[i-1][j]);
            }
        }

        return dp[n][n];
    }
};