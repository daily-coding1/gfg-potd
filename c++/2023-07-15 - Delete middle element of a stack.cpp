// Video Tutorial - https://youtu.be/VLBzp5Teckc

class Solution
{
    public:
    //Function to delete middle element of a stack.
    void deleteMid(stack<int>&s, int sizeOfStack)
    {
        removeAfterK(s, sizeOfStack/2);
    }
    
    void removeAfterK(stack<int>& s, int k) {
        if(k == 0) {
            s.pop();
            return;
        }
        int temp = s.top();
        s.pop();
        removeAfterK(s, k-1);
        s.push(temp);
    }
};