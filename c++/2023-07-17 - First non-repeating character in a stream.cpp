// Video Tutorial - https://youtu.be/_6uihC8dotY

class Solution {
	public:
		string FirstNonRepeating(string A){
		    unordered_map<char, int> mp;
		    queue<char> q;
		    string ans = "";
		    for(char c : A) {
		        ++mp[c];
		        if(mp[c] == 1) q.push(c);
		        while(!q.empty() && mp[q.front()] != 1) q.pop();
		        ans += q.empty() ? '#' : q.front();
		    }
		    return ans;
		}

};