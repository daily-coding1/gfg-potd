// Video Tutorial - https://youtu.be/_6uihC8dotY

class Solution
{
    public String FirstNonRepeating(String A)
    {
        HashMap<Character, Integer> mp = new HashMap<>();
	    Queue<Character> q = new LinkedList<>();
	    StringBuilder ans = new StringBuilder();
	    for(int i = 0; i < A.length(); i++) {
	        Character c = A.charAt(i);
	        if(!mp.containsKey(c)) mp.put(c, 0);
	        mp.put(c, mp.get(c) + 1); // Increment the count
	        if(mp.get(c) == 1) q.add(c);
	        while(!q.isEmpty() && mp.get(q.peek()) != 1) q.remove();
	        ans.append(q.isEmpty() ? '#' : q.peek());
	    }
	    return ans.toString();
    }
}