// Video Tutorial - https://youtu.be/-yfAETHu56s

class Solution
{
    //Function to remove duplicates from unsorted linked list.
    public Node removeDuplicates(Node head) 
    {
        Node dummy = new Node(0);
        dummy.next = head;
        Node cur = head, prev = dummy;
        HashSet<Integer> seen = new HashSet<>();
        while(cur != null) {
            Node nextCur = cur.next;
            if(seen.contains(cur.data)) {
                removeNode(prev);
            } else {
                seen.add(cur.data);
                prev = cur;
            }
            cur = nextCur;
        }
        Node ans = dummy.next;
        dummy.next = null; 
        return ans;
    }
    
    void removeNode(Node prev) {
        Node cur = prev.next;
        prev.next = cur.next;
        cur.next = null;
    }
}