// Video Tutorial - https://youtu.be/emyKW9gaobI

class Solution
{
    //Function to find the first non-repeating character in a string.
    static char nonrepeatingCharacter(String S)
    {
        HashMap<Character, Integer> countMap = new HashMap<>();
        for(int i = 0; i < S.length(); i++) {
            char c = S.charAt(i);
            countMap.put(c, countMap.containsKey(c) ? countMap.get(c) + 1 : 1);
        }
        for(int i = 0; i < S.length(); i++) {
            char c = S.charAt(i);
            if(countMap.get(c) == 1) return c;
        }
        return '$';
    }
}