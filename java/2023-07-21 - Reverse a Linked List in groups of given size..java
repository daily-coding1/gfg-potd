// Video Tutorial - https://youtu.be/_-eUsYGQuMM

class Solution
{
    public static Node reverse(Node node, int k)
    {
        Node cur = node, prevHead = null, head = null;
        while(cur != null) {
            Node tail = moveForward(cur, k-1);
            Node nextCur = tail.next;
            tail.next = null;
            reverseList(cur);
            if(prevHead != null) prevHead.next = tail;
            else head = tail;
            prevHead = cur;
            cur = nextCur;
        }
        return head;
    }
    
    // node must be non-NULL
    private static Node moveForward(Node node, int steps) {
        while(node.next != null && steps-- > 0) {
            node = node.next;
        }
        return node;
    }
    
    private static void reverseList(Node head) {
        Node prev = null;
        while(head != null) {
            Node next = head.next;
            head.next = prev;
            prev = head;
            head = next;
        }
    }
}