// Video Tutorial - https://youtu.be/evf4T-OR9kI

class Solution
{
    public int LongestRepeatingSubsequence(String str)
    {
        int n = str.length();
	    int[][] dp = new int[n+1][n+1];
	    // dp[i][j] = answer for s1 of length i and s2 of length j
	    for(int j = 0; j <= n; j++) dp[0][j] = 0;
	    for(int i = 0; i <= n; i++) dp[i][0] = 0;
	    for(int i = 1; i <= n; i++) {
	        for(int j = 1; j <= n; j++) {
	            dp[i][j] = str.charAt(i-1) == str.charAt(j-1) && i != j
	                        ? dp[i-1][j-1] + 1
	                        : Math.max(dp[i-1][j], dp[i][j-1]);
	        }
	    }
	    return dp[n][n];
    }
}