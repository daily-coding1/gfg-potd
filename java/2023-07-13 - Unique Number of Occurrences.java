// Video Tutorial - https://youtu.be/wA5qpFU0NrE

class Solution {
    public static boolean isFrequencyUnique(int n, int[] arr) {
        HashMap<Integer, Integer> freq = new HashMap<>();
        for(int i = 0; i < n; i++) {
            if(!freq.containsKey(arr[i])) freq.put(arr[i], 0);
            // At this point, freq HashMap will contain arr[i] as key for sure.
            freq.put(arr[i], freq.get(arr[i]) + 1);
        }
        HashSet<Integer> seen = new HashSet<>();
        for(int val : freq.values()) {
            // if val is present in seen
            if(seen.contains(val)) return false;
            seen.add(val);
        }
        return true;
    }
}
        