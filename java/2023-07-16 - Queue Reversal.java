// Video Tutorial - https://youtu.be/_M_75H_QDqw

class GfG{
    //Function to reverse the queue.
    public Queue<Integer> rev(Queue<Integer> q){
        Stack<Integer> s = new Stack<>();
        while(!q.isEmpty()) {
            int el = q.poll();
            s.push(el);
        }
        while(!s.isEmpty()) {
            int el = s.pop();
            q.offer(el);
        }
        return q;
    }
}