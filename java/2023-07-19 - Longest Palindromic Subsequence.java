// Video Tutorial - https://youtu.be/TH2-7OJKwPo

class Solution
{
    public int longestPalinSubseq(String S)
    {
        int n = S.length();
        String s1 = S;
        String s2 = new StringBuilder(S).reverse().toString();
        int[][] dp = new int[n+1][n+1];
        // dp[i][j] : Length of the longest subsequence common in s1[0...i-1] ans s2[0...j-1]
        
        // dp[i][j] = 1 + dp[i-1][j-1], if last character of s1[0...i-1] is same as that of s1[0...j-1]
        //          = max(dp[i-1][j], dp[i][j-1]), otherwise

        // Base case
        for(int i = 0; i <= n; i++) dp[i][0] = 0;
        for(int j = 0; j <= n; j++) dp[0][j] = 0;

        for(int i = 1; i <= n; i++) {
            for(int j = 1; j <= n; j++) {
                dp[i][j] = s1.charAt(i-1) == s2.charAt(j-1) ? 1 + dp[i-1][j-1] : Math.max(dp[i][j-1], dp[i-1][j]);
            }
        }
        
        return dp[n][n];
    }
}